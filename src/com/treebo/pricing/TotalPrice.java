package com.treebo.pricing;

public class TotalPrice {

    private String base_price;

    private String sell_price;

    private String pretax_price;

    private String tax;

    private String coupon_discount;

    private String autopromo_price_without_tax;

    private String base_price_tax;

    private String rack_rate;

    private String promo_discount;

    public String getBase_price ()
    {
        return base_price;
    }

    public void setBase_price (String base_price)
    {
        this.base_price = base_price;
    }

    public String getSell_price ()
    {
        return sell_price;
    }

    public void setSell_price (String sell_price)
    {
        this.sell_price = sell_price;
    }

    public String getPretax_price ()
    {
        return pretax_price;
    }

    public void setPretax_price (String pretax_price)
    {
        this.pretax_price = pretax_price;
    }

    public String getTax ()
    {
        return tax;
    }

    public void setTax (String tax)
    {
        this.tax = tax;
    }

    public String getCoupon_discount ()
    {
        return coupon_discount;
    }

    public void setCoupon_discount (String coupon_discount)
    {
        this.coupon_discount = coupon_discount;
    }

    public String getAutopromo_price_without_tax ()
    {
        return autopromo_price_without_tax;
    }

    public void setAutopromo_price_without_tax (String autopromo_price_without_tax)
    {
        this.autopromo_price_without_tax = autopromo_price_without_tax;
    }

    public String getBase_price_tax ()
    {
        return base_price_tax;
    }

    public void setBase_price_tax (String base_price_tax)
    {
        this.base_price_tax = base_price_tax;
    }

    public String getRack_rate ()
    {
        return rack_rate;
    }

    public void setRack_rate (String rack_rate)
    {
        this.rack_rate = rack_rate;
    }

    public String getPromo_discount ()
    {
        return promo_discount;
    }

    public void setPromo_discount (String promo_discount)
    {
        this.promo_discount = promo_discount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [base_price = "+base_price+", sell_price = "+sell_price+", pretax_price = "+pretax_price+", tax = "+tax+", coupon_discount = "+coupon_discount+", autopromo_price_without_tax = "+autopromo_price_without_tax+", base_price_tax = "+base_price_tax+", rack_rate = "+rack_rate+", promo_discount = "+promo_discount+"]";
    }
}
			
			