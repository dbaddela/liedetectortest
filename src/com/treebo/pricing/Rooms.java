package com.treebo.pricing;

public class Rooms
{
    private Price price;

    private String room_type;

    private RoomConfigWisePrices[] room_config_wise_prices;

    public Price getPrice ()
    {
        return price;
    }

    public void setPrice (Price price)
    {
        this.price = price;
    }

    public String getRoom_type ()
    {
        return room_type;
    }

    public void setRoom_type (String room_type)
    {
        this.room_type = room_type;
    }

    public RoomConfigWisePrices[] getRoom_config_wise_prices ()
    {
        return room_config_wise_prices;
    }

    public void setRoom_config_wise_prices (RoomConfigWisePrices[] room_config_wise_prices)
    {
        this.room_config_wise_prices = room_config_wise_prices;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [price = "+price+", room_type = "+room_type+", room_config_wise_prices = "+room_config_wise_prices+"]";
    }
}
			
			