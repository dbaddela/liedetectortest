package com.treebo.pricing;

public class RoomConfigWisePrices {
	    private String[] room_config;

	    private DateWisePrices[] date_wise_prices;

	    private TotalPrice total_price;

	    public String[] getRoom_config ()
	    {
	        return room_config;
	    }

	    public void setRoom_config (String[] room_config)
	    {
	        this.room_config = room_config;
	    }

	    public DateWisePrices[] getDate_wise_prices ()
	    {
	        return date_wise_prices;
	    }

	    public void setDate_wise_prices (DateWisePrices[] date_wise_prices)
	    {
	        this.date_wise_prices = date_wise_prices;
	    }

	    public TotalPrice getTotal_price ()
	    {
	        return total_price;
	    }

	    public void setTotal_price (TotalPrice total_price)
	    {
	        this.total_price = total_price;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [room_config = "+room_config+", date_wise_prices = "+date_wise_prices+", total_price = "+total_price+"]";
	    }
	}
				
				