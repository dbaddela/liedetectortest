package com.treebo.promptcloud.ota.data;

import com.google.gson.annotations.SerializedName;

public class Record
{
	@SerializedName("hotel_id")
    private String hotelId;

    private String ota;

    @SerializedName("seq_id")
    private String seqId;

    @SerializedName("checkin_date")
    private String checkinDate;

    @SerializedName("crawled_date")
    private String crawledDate;

    @SerializedName("uniq_id")
    private String uniqId;

    @SerializedName("hotel_name")
    private String hotelName;

    @SerializedName("room_type")
    private RoomType[] roomType;

    public String getHotelId ()
    {
        return hotelId;
    }

    
    public void setHotelId (String hotel_id)
    {
        this.hotelId = hotel_id;
    }

    public String getOta ()
    {
        return ota;
    }

    public void setOta (String ota)
    {
        this.ota = ota;
    }

    public String getSeqId ()
    {
        return seqId;
    }

    
    public void setSeqId (String seq_id)
    {
        this.seqId = seq_id;
    }

    public String getCheckinDate ()
    {
        return checkinDate;
    }

    public void setCheckinDate (String checkin_date)
    {
        this.checkinDate = checkin_date;
    }

    public String getCrawledDate ()
    {
        return crawledDate;
    }

    
    public void setCrawledDate (String crawled_date)
    {
        this.crawledDate = crawled_date;
    }

    public String getUniqId ()
    {
        return uniqId;
    }

    
    public void setUniqId (String uniq_id)
    {
        this.uniqId = uniq_id;
    }

    public String getHotelName ()
    {
        return hotelName;
    }

    
    public void setHotelName (String hotel_name)
    {
        this.hotelName = hotel_name;
    }

    public RoomType[] getRoomType ()
    {
        return roomType;
    }

    public void setRoomType (RoomType[] room_type)
    {
        this.roomType = room_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [hotel_id = "+hotelId+", ota = "+ota+", seq_id = "+seqId+", checkin_date = "+checkinDate+", crawled_date = "+crawledDate+", uniq_id = "+uniqId+", hotel_name = "+hotelName+", room_type = "+roomType+"]";
    }
}