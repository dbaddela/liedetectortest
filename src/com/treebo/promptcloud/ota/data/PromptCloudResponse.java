package com.treebo.promptcloud.ota.data;

public class PromptCloudResponse {

	    private Root root;

	    public Root getRoot ()
	    {
	        return root;
	    }

	    public void setRoot (Root root)
	    {
	        this.root = root;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [root = "+root+"]";
	    }
	}