package com.treebo.promptcloud.ota.data;

public class Page {
	private String pageurl;

	private Record record;

	public String getPageurl() {
		return pageurl;
	}

	public void setPageurl(String pageurl) {
		this.pageurl = pageurl;
	}

	public Record getRecord() {
		return record;
	}

	public void setRecord(Record record) {
		this.record = record;
	}

	@Override
	public String toString() {
		return "ClassPojo [pageurl = " + pageurl + ", record = " + record + "]";
	}
}
