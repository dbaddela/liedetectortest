package com.treebo.promptcloud.api;

import javax.xml.bind.annotation.XmlAttribute;

public class Entry
{
	@XmlAttribute
    private String md5sum;

	@XmlAttribute
	private String updated_ts;

	@XmlAttribute
	private String updated;

	@XmlAttribute
	private String url;

    public String getMd5sum ()
    {
        return md5sum;
    }

    public void setMd5sum (String md5sum)
    {
        this.md5sum = md5sum;
    }

    public String getUpdated_ts ()
    {
        return updated_ts;
    }

    public void setUpdated_ts (String updated_ts)
    {
        this.updated_ts = updated_ts;
    }

    public String getUpdated ()
    {
        return updated;
    }

    public void setUpdated (String updated)
    {
        this.updated = updated;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [md5sum = "+md5sum+", updated_ts = "+updated_ts+", updated = "+updated+", url = "+url+"]";
    }
}
			
			