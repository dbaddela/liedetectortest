package com.treebo.utlis;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.GZIPInputStream;

public class Utils {

	/**
	 * Extract the .gz input file 
	 * 
	 * @param inFile
	 * @param outFile
	 */
	public static void gunzipI̋t(String inFile, String outFile) {

		byte[] buffer = new byte[1024];

		try {

			GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(inFile));

			FileOutputStream out = new FileOutputStream(outFile);

			int len;
			while ((len = gzis.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}

			gzis.close();
			out.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * 
	 * Code to download file from url
	 * @param urlString
	 * @throws IOException
	 */
	public static void downloadFromUrl(String urlString) throws IOException
	{
		 
		 String fileName = "/tmp/temp/" + urlString.substring(urlString.lastIndexOf("/")+1);//"file.gz"; //The file that will be saved on your computer
		 URL link = new URL(urlString); //The file that you want to download
			
	     //Code to download
		 InputStream in = new BufferedInputStream(link.openStream());
		 ByteArrayOutputStream out = new ByteArrayOutputStream();
		 byte[] buf = new byte[1024];
		 int n = 0;
		 while (-1!=(n=in.read(buf)))
		 {
		    out.write(buf, 0, n);
		 }
		 out.close();
		 in.close();
		 byte[] response = out.toByteArray();

		 FileOutputStream fos = new FileOutputStream(fileName);
		 fos.write(response);
		 fos.close();

	}
}
