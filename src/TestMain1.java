import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gson.Gson;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.xml.XmlPath;
import com.treebo.promptcloud.ota.data.Page;
import com.treebo.promptcloud.ota.data.PromptCloudResponse;
import com.treebo.promptcloud.ota.data.Record;
import com.treebo.promptcloud.ota.data.RoomType;
import com.treebo.utlis.Utils;

public class TestMain1 {
	public static void _main(String[] args)
	{
		
		System.out.println(firstRepeatedWord("He had had    quite		 enough of this nonsense."));
	}
	
	public static int maxMoney(int n, long k) {
		int sum = 0;
		for(int i=1,j=1; i<=n; i++)
		    {
		    sum+=i;
		    if(sum == k)
		        {
		        sum -= j;
		        j++;
		    }
		}
		    return sum;
		}
	
	public static String firstRepeatedWord(String s) {
	    s = s.replace(",", "_").replace(":", "_").replace(";","_").replace("-","_").replace(".","_").replace(" ", "_");
	    String []toks = s.split("_");
	    HashSet<String> hs = new HashSet<String>();
	    int i=0;
	    for( i=0; i<toks.length-1; i++)
	        {
	        if(hs.contains(toks[i]))
	            {
	            break;
	        }
	        else
	            hs.add(toks[i]);
	    }
	        if(i == toks.length)
	            return null;
	        else
	            return toks[i];
	    }
	public static void main(String[] args) throws Exception {

		HashMap<String, HashMap<String, String>> hotelIdToOtaDataMap = getTreeboHotelIdToOtaMappingData();
		
		XmlPath xmlPath = RestAssured.given().relaxedHTTPSValidation()
				.get("https://api.promptcloud.com/v2/data/info?id=treeb_482bfc094b&client_auth_key=xESPq95LOv-tmJtBApT27QpDyVxEGM1jNH5Otl0BxKQ&from_date=20170320")
				.andReturn().xmlPath();
		List<String> urlList = xmlPath.getList("root.entry.url");

		for (int i = urlList.size() - 1; i >= 0; i--) {
			Utils.downloadFromUrl(urlList.get(i));

		}

		List<String> fileList = getFilesForFolder(new File("/tmp/temp/"));
		for (String file : fileList) {
			Process p = Runtime.getRuntime().exec("gunzip " + "/tmp/temp/" + file);
		}

		HashMap<String, String> hm = getOtaHotelCodeTreeboHotelCodeMap();
		
		for (String key : hm.keySet()) {
			System.out.println(key + " : " + hm.get(key));
		}

		// List<String> extractedFileList = getFilesForFolder(new
		// File("/tmp/temp/"));
		Gson gson = new Gson();
		// PromptCloudResponse resp = gson.fromJson(new FileReader("/tmp/temp/"
		// + extractedFileList.get(0)),
		// PromptCloudResponse.class);
		// // resp.getRoot().getPage().length;
		// System.out.println("Length: " + resp.getRoot().getPage().length);

		HashMap<String, List<LinkedHashMap<String, Object>>> outMap = new HashMap<String, List<LinkedHashMap<String, Object>>>();

		List<String> fileList1 = getFilesForFolder(new File("/tmp/temp/"));
		for (String file : fileList1) {
			PromptCloudResponse resp = gson.fromJson(new FileReader("/tmp/temp/" + file), PromptCloudResponse.class);
			Page[] pages = resp.getRoot().getPage();

			for (Page page : pages) {
				LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
				List<LinkedHashMap<String, Object>> list = null;
				Record record = page.getRecord();

				map.put("checkin_date", record.getCheckinDate());
				map.put("hotel_name", record.getHotelName());
				map.put("ota", record.getOta());
				LinkedHashMap<String, String> rtMap = new LinkedHashMap<String, String>();

				RoomType[] roomTypes = record.getRoomType();
				if (roomTypes == null)
					map.put("room_type", null);
				else {
					for (RoomType rt : roomTypes) {
						rtMap.put(rt.getRoomTypeName(), rt.getRoomTypePrice());
					}
					map.put("room_type", rtMap);
				}

				if (!outMap.containsKey(page.getRecord().getSeqId())) {
					list = new ArrayList<LinkedHashMap<String, Object>>();
					list.add(map);
					outMap.put(page.getRecord().getSeqId(), list);
				} else {
					outMap.get(page.getRecord().getSeqId()).add(map);
				}
			}

		}
		
		
	/*	for (String key : hotelIdToOtaDataMap.keySet()) {
			Date date = new Date();
			LocalDateTime ldt = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			
			for (int i = 0; i < 30; i++) {
				String collatedOutput = key + ",";
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				ldt = ldt.plusDays(1);
				Date d = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
				String checkIn = sdf.format(d);

				for (int otaCode = 1; otaCode <= 2; otaCode++) {
						String otaHotelId = hotelIdToOtaDataMap.get(key).get(otaCode +"");
						if (outMap.containsKey(otaHotelId)) {
							List<LinkedHashMap<String, Object>> list = outMap.get(otaHotelId);
							for (LinkedHashMap<String, Object> lhm : list) {
								System.out.println(lhm.get("checkin_date"));
								if (lhm.get("checkin_date").equals(checkIn)) {
									collatedOutput += checkIn + "," + lhm.get("hotel_name") + ",";
									//collatedOutput += lhm.get
									LinkedHashMap<String, String> rtMap = (LinkedHashMap<String, String>) lhm.get("room_type");
									if(rtMap!= null)
									{
										String roomType = "";
										for (String rt : rtMap.keySet()) {
											if(rt.contains("Standard") && rt.contains("Breakfast"))
												roomType ="Oak";
											if(rt.contains("Deluxe") && rt.contains("Breakfast"))
												roomType ="Maple";
											if(rt.contains("Premium") && rt.contains("Breakfast"))
												roomType ="Mahagony";
											System.out.println(collatedOutput + roomType + "," + rtMap.get(rt).replace(",", ""));
										}
									}
									else{
										
									}
								} else {
									// No data for this checkIn date
								}
							}
						}

				}
			}
		}*/
		FileWriter fileWriter = null;

		try {
			fileWriter = new FileWriter("output.csv");

			// Write the CSV file header
			fileWriter.append("OTA_HOTEL_ID,CHECKIN_DATE,HOTEL_NAME,OTA,ROOM_TYPE,PRICE\n");
	
			// Add a new line separator after the header
			for (String otaHotelId : outMap.keySet()) {
				String output = otaHotelId + ",";
				for (HashMap<String, Object> hmo : outMap.get(otaHotelId)) {
					String temp1 = "";
					for (String key : hmo.keySet()) {
						if (key.equals("room_type")) {
							LinkedHashMap<String, String> rtMap = (LinkedHashMap<String, String>) hmo.get(key);
							if (rtMap != null) {
								for (String key1 : rtMap.keySet()) {
									System.out.println(output + temp1 + key1 + "," + rtMap.get(key1).replace(",", ""));
									fileWriter.append(
											output + temp1 + key1 + "," + rtMap.get(key1).replace(",", "") + "\n");
								}
							} else {
								System.out.println(output + temp1 + "," + null);
								fileWriter.append(output + temp1 + "," + null + "\n");
							}

						} else {
							temp1 += hmo.get(key) + ",";
						}
					}
				}
			}

			System.out.println("CSV file was created successfully !!!");

		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {

			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}

		}

		/*
		 * CloseableHttpClient httpClient = HttpClients.createDefault(); String
		 * url =
		 * "http://pricing.treebohotels.com/v1/price?hotel_id=22&stay_start=2017-04-02&stay_end=2017-04-03&booking_date=2017-03-10&room_config=1-0";
		 * HttpGet httpGet = new HttpGet(url);
		 * 
		 * CloseableHttpResponse response = httpClient.execute(httpGet);
		 * 
		 * String jsonString = EntityUtils.toString(response.getEntity());
		 * PricingServiceResponse psr = gson.fromJson(jsonString,
		 * PricingServiceResponse.class);
		 */

		System.out.println("End");
	}

	public static List<String> getFilesForFolder(final File folder) {
		List<String> fileList = new ArrayList<>();
		for (final File fileEntry : folder.listFiles()) {
			fileList.add(fileEntry.getName());
		}

		return fileList;
	}

	public static HashMap<String, HashMap<String, String>> getTreeboHotelIdToOtaMappingData() {
		Connection connection = null;
		HashMap<String, HashMap<String,String>> hm = new HashMap<String, HashMap<String,String>>();
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://172.40.10.229:5430/lie_detector", "treebo",
					"treebo");

		} catch (SQLException e) {

		} catch (ClassNotFoundException e) {
			System.out.println("postgresql drive not found");
		}

		if (connection != null) {
			try {
				Statement stmt = connection.createStatement();
				String sql = "select hotel_code_mapping_id, ota_hotel_id, ota_mapping_id from ld_ota_hotel_name_id_mapping";
				ResultSet rs = stmt.executeQuery(sql);
				while (rs.next()) {
					String hotelId = rs.getString(1);
					String otaHotelId = rs.getString(2);
					String otaCode = rs.getString(3);
					if(hm.containsKey(hotelId))
					{
						hm.get(hotelId).put(otaCode, otaHotelId);
					}
					else
					{
						HashMap<String, String> temp = new HashMap<String, String>();
						temp.put(otaCode, otaHotelId);
						hm.put(hotelId, temp);
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			System.out.println("Failed to make connection!");
		}

		return hm;

	}

	public static HashMap<String, String> getOtaHotelCodeTreeboHotelCodeMap() {
		Connection connection = null;
		HashMap<String, String> hm = new HashMap<String, String>();
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://172.40.10.229:5430/lie_detector", "treebo",
					"treebo");

		} catch (SQLException e) {

		} catch (ClassNotFoundException e) {
			System.out.println("postgresql drive not found");
		}

		if (connection != null) {
			try {
				Statement stmt = connection.createStatement();
				String sql = "select ota_hotel_id, hotel_code_mapping_id from ld_ota_hotel_name_id_mapping";
				ResultSet rs = stmt.executeQuery(sql);
				while (rs.next()) {
					hm.put(rs.getString(1), rs.getString(2));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			System.out.println("Failed to make connection!");
		}

		return hm;

	}


}