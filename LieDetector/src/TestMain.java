import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.net.ssl.HttpsURLConnection;

public class TestMain {
    public static void _main(String args[]) throws Exception {
        String ftpUrl = "http://wwwccc.zip";
        URL url = new URL(ftpUrl);
        unpackArchive(url);
    }

    public static void unpackArchive(URL url) throws IOException {
    	url = new URL("http://datav2.promptcloud.com/data//treeb_482bfc094b/ota_hotel_prices_treebo/makemytrip_com_ota_hotel_prices_treebo_deduped_n-20170311_064645430577824.json.gz");
        
        InputStream in = ((HttpURLConnection)url.openConnection()).getInputStream();
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", "Mozilla/5.0");

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
		
		BufferedReader br = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = br.readLine()) != null) {
			response.append(inputLine);
		}
		br.close();

		//print result
		System.out.println(response.toString());
    }
}