package com.treebo.pricing;


public class Hotels
{
private String hotel_id;

private Rooms[] rooms;

public String getHotel_id ()
{
return hotel_id;
}

public void setHotel_id (String hotel_id)
{
this.hotel_id = hotel_id;
}

public Rooms[] getRooms ()
{
return rooms;
}

public void setRooms (Rooms[] rooms)
{
this.rooms = rooms;
}

@Override
public String toString()
{
return "ClassPojo [hotel_id = "+hotel_id+", rooms = "+rooms+"]";
}
}
