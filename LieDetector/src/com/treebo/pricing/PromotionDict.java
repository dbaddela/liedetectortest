package com.treebo.pricing;

public class PromotionDict {
	    private String id;

	    private String unit;

	    private String description;

	    private String value;

	    public String getId ()
	    {
	        return id;
	    }

	    public void setId (String id)
	    {
	        this.id = id;
	    }

	    public String getUnit ()
	    {
	        return unit;
	    }

	    public void setUnit (String unit)
	    {
	        this.unit = unit;
	    }

	    public String getDescription ()
	    {
	        return description;
	    }

	    public void setDescription (String description)
	    {
	        this.description = description;
	    }

	    public String getValue ()
	    {
	        return value;
	    }

	    public void setValue (String value)
	    {
	        this.value = value;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [id = "+id+", unit = "+unit+", description = "+description+", value = "+value+"]";
	    }
	}
				
				