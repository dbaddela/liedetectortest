package com.treebo.pricing;

public class DateWisePrices {
	    private DateWisePricesPrice price;

	    private String date;

	    public DateWisePricesPrice getPrice ()
	    {
	        return price;
	    }

	    public void setPrice (DateWisePricesPrice price)
	    {
	        this.price = price;
	    }

	    public String getDate ()
	    {
	        return date;
	    }

	    public void setDate (String date)
	    {
	        this.date = date;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [price = "+price+", date = "+date+"]";
	    }
	}