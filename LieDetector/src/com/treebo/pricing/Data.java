package com.treebo.pricing;

public class Data
{
    private String stay_end;

    private String booking_date;

    private String stay_start;

    private String type;

    private String currency_code;

    private Hotels[] hotels;

    public String getStay_end ()
    {
        return stay_end;
    }

    public void setStay_end (String stay_end)
    {
        this.stay_end = stay_end;
    }

    public String getBooking_date ()
    {
        return booking_date;
    }

    public void setBooking_date (String booking_date)
    {
        this.booking_date = booking_date;
    }

    public String getStay_start ()
    {
        return stay_start;
    }

    public void setStay_start (String stay_start)
    {
        this.stay_start = stay_start;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getCurrency_code ()
    {
        return currency_code;
    }

    public void setCurrency_code (String currency_code)
    {
        this.currency_code = currency_code;
    }

    public Hotels[] getHotels ()
    {
        return hotels;
    }

    public void setHotels (Hotels[] hotels)
    {
        this.hotels = hotels;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [stay_end = "+stay_end+", booking_date = "+booking_date+", stay_start = "+stay_start+", type = "+type+", currency_code = "+currency_code+", hotels = "+hotels+"]";
    }
}