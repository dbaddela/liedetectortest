package com.treebo.promptcloud.api;

public class PromptCloudApiResponse {
	    private Root root;

	    public Root getRoot ()
	    {
	        return root;
	    }

	    public void setRoot (Root root)
	    {
	        this.root = root;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [root = "+root+"]";
	    }
	}