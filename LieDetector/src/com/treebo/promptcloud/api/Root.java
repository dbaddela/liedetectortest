package com.treebo.promptcloud.api;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Root {
	@XmlAttribute
    private String updated_ts;

	@XmlAttribute
	private String updated;

	@XmlAttribute
	private String status;

	@XmlAttribute
	private String status_desc;

	@XmlAttribute
	private String next_query;

	@XmlAttribute
	private Entry[] entry;

    public String getUpdated_ts ()
    {
        return updated_ts;
    }

    public void setUpdated_ts (String updated_ts)
    {
        this.updated_ts = updated_ts;
    }

    public String getUpdated ()
    {
        return updated;
    }

    public void setUpdated (String updated)
    {
        this.updated = updated;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getStatus_desc ()
    {
        return status_desc;
    }

    public void setStatus_desc (String status_desc)
    {
        this.status_desc = status_desc;
    }

    public String getNext_query ()
    {
        return next_query;
    }

    public void setNext_query (String next_query)
    {
        this.next_query = next_query;
    }

    public Entry[] getEntry ()
    {
        return entry;
    }

    public void setEntry (Entry[] entry)
    {
        this.entry = entry;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [updated_ts = "+updated_ts+", updated = "+updated+", status = "+status+", status_desc = "+status_desc+", next_query = "+next_query+", entry = "+entry+"]";
    }
}
	