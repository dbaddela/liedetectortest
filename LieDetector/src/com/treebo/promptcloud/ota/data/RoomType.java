package com.treebo.promptcloud.ota.data;

import com.google.gson.annotations.SerializedName;

public class RoomType {

		@SerializedName("room_type_price")
	    private String roomTypePrice;

		@SerializedName("room_type_name")
	    private String roomTypeName;

	    public String getRoomTypePrice ()
	    {
	        return roomTypePrice;
	    }

	    
	    public void setRoomTypePrice (String room_type_price)
	    {
	        this.roomTypePrice = room_type_price;
	    }

	    public String getRoomTypeName ()
	    {
	        return roomTypeName;
	    }

	    
	    public void setRoomTypeName (String room_type_name)
	    {
	        this.roomTypeName = room_type_name;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [room_type_price = "+roomTypePrice+", room_type_name = "+roomTypeName+"]";
	    }
	}