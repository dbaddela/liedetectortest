package com.treebo.promptcloud.ota.data;

public class Root
{
    private Page[] page;

    public Page[] getPage ()
    {
        return page;
    }

    public void setPage (Page[] page)
    {
        this.page = page;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [page = "+page+"]";
    }
}